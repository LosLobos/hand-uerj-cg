﻿#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include "Hand.h"

float angleCameraX = 0;
float angleCameraY = 0;
float angleCameraZ = 0;

float zoom = -15.0f;

void init(void)
{
	//	LoadGLTextures();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0);					// Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LEQUAL);				// The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST);			// Enables Depth Testing
	glShadeModel(GL_SMOOTH);			// Enables Smooth Color Shading
}

Hand hand(1.0);

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat			diffuseLight[] = { 1.0f, 1.0f, 1.0f, 1.0 };
	GLfloat			ambientLight[] = { 0.2f, 0.2f, 0.4f, 1.0 };
	GLfloat			lightPos[] = { 0.0f, 500.0f, 100.0f, 1.0f };
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, diffuseLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(1.0f, 1.0f, 1.0f);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, diffuseLight);
	glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, 50);

	glPushMatrix();

	glTranslatef(0.0, -5.0, zoom);
	glRotatef(angleCameraZ, 0.0, 0.0, 1.0);
	glRotatef(angleCameraY, 0.0, 1.0, 0.0);
	glRotatef(angleCameraX, 1.0, 0.0, 0.0);
	glColor3f(0.16, 1.52, 1.32);

	// Draw Hand with its fingers.
	hand.draw();

	glPopMatrix();

	glutSwapBuffers();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(55.0, (GLfloat)w / (GLfloat)h, 1.0, 400.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -10.0);
}

void idle()
{
}

void keyboard(unsigned char key, int x, int y)
{
	float angle = 0;
	int fingerNumber = -1;

	switch (key)
	{
	case 'q':
		while (hand.getAngle(0, 0) + angle <= 100) {
			angle += 0.01;
			for (int i = 0; i < hand.getFingersNumber(); i++) {
				hand.setAngle(0, i, hand.getAngle(0, i) + angle);
				display();
			}
		}
		break;
	case 'w':
		while (hand.getAngle(0, 0) + angle >= 0) {
			angle -= 0.01;
			for (int i = 0; i < hand.getFingersNumber(); i++) {
				hand.setAngle(0, i, hand.getAngle(0, i) + angle);
				display();
			}
		}
		break;
	case 'e':
		angle = 5;
		fingerNumber = Finger::LITTLE_FINGER;
		break;
	case 'd':
		angle = -5;
		fingerNumber = Finger::LITTLE_FINGER;
		break;
	case 'r':
		angle = 5;
		fingerNumber = Finger::RING_FINGER;
		break;
	case 'f':
		angle = -5;
		fingerNumber = Finger::RING_FINGER;
		break;
	case 't':
		angle = 5;
		fingerNumber = Finger::MIDDLE_FINGER;
		break;
	case 'g':
		angle = -5;
		fingerNumber = Finger::MIDDLE_FINGER;
		break;
	case 'y':
		angle = 5;
		fingerNumber = Finger::INDEX_FINGER;
		break;
	case 'h':
		angle = -5;
		fingerNumber = Finger::INDEX_FINGER;
		break;
	case 'u':
		angle = 5;
		fingerNumber = Finger::THUMB_FINGER;
		break;
	case 'j':
		angle = -5;
		fingerNumber = Finger::THUMB_FINGER;
		break;
	case 'z':
		angleCameraX = angleCameraX + 5;
		break;
	case 'x':
		angleCameraY = angleCameraY + 5;
		break;
	case 'c':
		angleCameraZ = angleCameraZ + 5;
		break;
	case 'n':
		zoom = zoom - 5;
		break;
	case 'm':
		zoom = zoom + 5;
		break;
	default:
		return;
	}

	if (fingerNumber >= 0 && hand.getAngle(0, fingerNumber) + angle <= 100 && hand.getAngle(0, fingerNumber) + angle >= 0)
		hand.setAngle(0, fingerNumber, hand.getAngle(0, fingerNumber) + angle);

	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(300, 300);
	glutInitWindowPosition(200, 200);
	glutCreateWindow(argv[0]);
	init();

	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}