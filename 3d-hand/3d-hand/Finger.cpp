#include "Finger.h"

void Finger::draw()
{
	if (amountOfBones > 0)
		fingerBones[0]->draw();
}
void Finger::setAngle(float angle)
{
	fingerBones[0]->setAngle(angle * 0.9);
}

float Finger::getAngle()
{
	return fingerBones[0]->getAngle();
}

Bone ** Finger::getFingerBones()
{
	return this->fingerBones;
}

int Finger::getAmountOfFingerBones()
{
	return this->amountOfBones;
}


