#include "Hand.h"

Hand::Hand() : Hand(1.0) {}

Hand::Hand(float size) : size(size), numberOfFingers(5)
{
	this->fingers = new Finger *[this->numberOfFingers];

	Finger * littleFinger = new Finger(4 * size, size - 0.2);
	Finger * ringFinger = new Finger(4 * size, size - 0.2);
	Finger * middleFinger = new Finger(5 * size, size - 0.2);
	Finger * indexFinger = new Finger(3.5 * size, size - 0.2);
	Finger * thumbFinger;

	Bone ** thumbBones = new Bone *[2];

	for (int i = 0; i < 2; i++) {
		thumbBones[i] = new Bone(0.5 * 4 * size, size - 0.3, !i==1);
	}

	thumbFinger = new Finger(4 * size, size - 0.3, 2, thumbBones);

	this->fingers[Finger::LITTLE_FINGER] = littleFinger;
	this->fingers[Finger::RING_FINGER] = ringFinger;
	this->fingers[Finger::MIDDLE_FINGER] = middleFinger;
	this->fingers[Finger::INDEX_FINGER] = indexFinger;
	this->fingers[Finger::THUMB_FINGER] = thumbFinger;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 5; j++)
			angle[i][j] = 0;
}

void Hand::setAngle(int joint, int finger, float angle)
{
	this->angle[joint][finger] = angle;

	this->fingers[finger]->setAngle(angle);
}

float Hand::getAngle(int joint, int finger)
{
	return angle[joint][finger];
}

int Hand::getFingersNumber()
{
	return this->numberOfFingers;
}

void Hand::draw()
{
	glPushMatrix();
	glTranslatef(0.0, 6.0*size, 0.0);

	// Little Finger and Joint Setup
	glPushMatrix();
	glTranslatef(-3 * size, size - 0.5 , 0.0);
	glutSolidCube(1);
	glRotatef(angle[0][0] * 0.9, 1.0, 0.0, 0.0);
	// Draws Little Finger
	this->fingers[Finger::LITTLE_FINGER]->draw();

	glPopMatrix();

	// Ringer Finger and Joint Setup
	glPushMatrix();
	glTranslatef(-1.5*size, size + 0.1, 0.0);
	glutSolidCube(1);
	glRotatef(angle[0][1] * 0.9, 1.0, 0.0, 0.0);
	// Draw Ring Finger
	this->fingers[Finger::RING_FINGER]->draw();
	
	glPopMatrix();

	// Middle Finger and Joint Setup
	glPushMatrix();
	glTranslatef(0, size, 0.0);
	glutSolidCube(1);
	glRotatef(angle[0][2] * 0.9, 1.0, 0.0, 0.0);
	// Draw Middle Finger
	this->fingers[Finger::MIDDLE_FINGER]->draw();
	
	glPopMatrix();

	// Index Finger and Joint Setup
	glPushMatrix();
	glTranslatef(1.5*size, size - 0.5, 0.0);
	glutSolidCube(1);
	glRotatef(angle[0][3] * 0.9, 1.0, 0.0, 0.0);
	glRotatef(angle[1][3] * 0.9, 1.0, 0.0, 0.0);
	glRotatef(angle[2][3] * 0.9, 0.0, 0.0, 1.0);
	// Draw Index Finger
	this->fingers[Finger::INDEX_FINGER]->draw();
	
	glPopMatrix();

	// Thumb Finger and Joint Setup
	glPushMatrix();
	glTranslatef(3 * size, -4 * size, 0.0);
	glRotatef(-80, 0.0, 0.0, 1.0);
	glRotatef(-20, 0.0, 1.0, 0.0);
	glRotatef(angle[0][4] * 0.5, 1.0, 0.0, 0.0);
	glScalef(1.5, 1, 1);
	glutSolidCube(1);
	// Draw Thumb Finger
	this->fingers[Finger::THUMB_FINGER]->draw();
	glPopMatrix();

	// Go Back to the Second Matrix ( The palm of the Hand )
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-0.75*size, 3.0*size, 0.0);
	glScalef(5.5*size, 6.0*size, 1.25*size);
	glutSolidSphere(size / 1.4, 10, 10);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.80*size, 0.01*size, 0.0);
	glScalef(1.3*size, 2.0*size, 1.25*size);
	glutSolidCube(2);
	glPopMatrix();

	// Draw the Ground beneath the Hand
	glColor3f(2.05, 0.97, 0.51);
	glPushMatrix();
	glTranslatef(-0.0*size, -5.00*size, 0.0);
	glScalef(2.5*size, 0.5*size, 1.25*size);
	glutSolidCube(5);
	glPopMatrix();
	
}

Hand::~Hand()
{
}
