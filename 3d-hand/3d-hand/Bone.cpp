#include "Bone.h"

void Bone::draw()
{
	glPushMatrix();

	glTranslatef(0.0, length / 2.0, 0.0);

	glPushMatrix();

	glScalef(width, length, width);
	glutSolidCube(width + 0.5);

	glPopMatrix();

	glTranslatef(0.0, length / 2.0, 0.0);

	// Check if have more Bones to Draw in the Finger
	if (this->connection)
	{
		glRotatef(angle, 1.0, 0.0, 0.0);
		this->connection->draw();              
	}

	glPopMatrix();
}

void Bone::setConnection(Bone * bone, float angle)
{
	this->connection = bone;
	this->angle = angle;
}

void Bone::setConnection(Bone * bone)
{
	this->connection = bone;
	this->angle = 0.0f;
}

void Bone::setAngle(float angle)
{
	this->angle = angle;
}

float Bone::getAngle()
{
	return this->angle;
}


Bone::~Bone()
{
}
