#pragma once
#include "Finger.h"
#include <stdio.h>
#include <GL/glut.h>
#include <GL/gl.h>

//Finger littleFinger(2, 1);

class Hand
{
public:

	float size;

	float angle[3][5];

	Finger ** fingers;
	int numberOfFingers;

	Hand();
	Hand(float size);

	void draw();

	void setAngle(int joint, int finger, float value);
	float getAngle(int joint, int finger);
	int getFingersNumber();

	virtual ~Hand();
};

