#pragma once
#include <GL/glut.h>
#include <GL/gl.h>
class Bone
{
public:

	Bone(float length, float width, bool articulable) : length(length), width(width), articulable(articulable), connection(false) {}

	void draw();

	void setConnection(Bone * bone, float angle);

	void setConnection(Bone * bone);

	void setAngle(float angle);

	float getAngle();

	virtual ~Bone();

private:
	float width;
	float length;
	float angle;
	bool articulable;

	// Define connection between bones
	Bone * connection;
};

