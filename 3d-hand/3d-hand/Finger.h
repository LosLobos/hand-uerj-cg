#pragma once
#include "Joint.h"
#include "Bone.h"

class Finger
{
public:

	static const int LITTLE_FINGER = 0;
	static const int RING_FINGER = 1;
	static const int MIDDLE_FINGER = 2;
	static const int INDEX_FINGER = 3;
	static const int THUMB_FINGER = 4;

	Finger(float length, float width) : Finger(length, width, 0, NULL) {}

	Finger(float length, float width, int bones, Bone ** fingerBones) {

		if (fingerBones == NULL) {

			this->amountOfBones = 3;

			this->fingerBones = new Bone *[this->amountOfBones];

			float boneParams[3] = { 0.4, 0.35, 0.25 };

			for (int i = 0; i < this->amountOfBones; i++) {
				bool articulable = !(i == 2);
				this->fingerBones[i] = new Bone(length * boneParams[i], width, articulable);
			}
		}
		else {
			this->fingerBones = fingerBones;
			this->amountOfBones = bones;
		}

		for (int i = 0; i < this->amountOfBones - 1; i++) {
			
			Bone * actualBone = this->fingerBones[i];
			Bone * nextBone = this->fingerBones[i+1];

			actualBone->setConnection(nextBone);
		}
	}
	
	void draw();

	void setAngle(float angle);
	float getAngle();

	Bone ** getFingerBones();

	int getAmountOfFingerBones();

private:
	int amountOfBones;

	// Define how many Parts the Finger will have
	Bone ** fingerBones;
};

